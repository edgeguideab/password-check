'use strict';
let commonPasswords = require('./passwords.json').list;

module.exports = {
  evaluate,
  score: scorePassword
};

function evaluate(password, options) {
  options = typeof options === 'object' ? options : {};
  let minLength = options.length || 8;

  if (password.length < minLength) {
    return {
      valid: false,
      reason: 'TOO_SHORT'
    };
  }

  if (!options.allowCommon) {
    if (!options.strict) {
      if (commonPasswords.indexOf(password) !== -1) {
        return {
          valid: false,
          reason: 'TOO_COMMON'
        };
      }
    } else {
      for (let i = 0; i < commonPasswords.length; i++) {
        if (
          password.toLowerCase().indexOf(commonPasswords[i].toLowerCase()) !==
          -1
        ) {
          return {
            valid: false,
            reason: 'CONTAINS_COMMON_PATTERNS'
          };
        }
      }
    }
  }
  options.upperCase = options.upperCase || options.capital;
  if (options.upperCase) {
    let matches = (password.match(/[A-Z]/g) || []).length;

    if (matches < options.upperCase) {
      return {
        valid: false,
        reason: 'TOO_FEW_CAPITAL_LETTERS'
      };
    }
  }

  options.lowerCase = options.lowerCase || options.small;
  if (options.lowerCase) {
    let matches = (password.match(/[a-z]/g) || []).length;

    if (matches < options.lowerCase) {
      return {
        valid: false,
        reason: 'TOO_FEW_SMALL_LETTERS'
      };
    }
  }

  if (options.numeric) {
    let matches = (password.match(/[0-9]/g) || []).length;

    if (matches < options.numeric) {
      return {
        valid: false,
        reason: 'TOO_FEW_NUMERIC_CHARACTERS'
      };
    }
  }

  if (options.nonAlphanumeric) {
    let matches = (password.match(/[^0-9a-zA-Z]/g) || []).length;

    if (matches < options.numeric) {
      return {
        valid: false,
        reason: 'TOO_FEW_NON_ALPHANUMERIC_CHARACTERS'
      };
    }
  }

  if (options.minScore && !isNaN(options.minScore)) {
    let score = scorePassword(password);
    if (score < options.minScore) {
      return {
        valid: false,
        reason: 'SCORE_TOO_LOW',
        score: score
      };
    }
  }

  return {
    valid: true
  };
}

function scorePassword(password, options) {
  options = typeof options === 'object' ? options : {};
  let score = 0;
  let alphaLowerValue = 0.1;
  let alphaUpperValue = 0.2;
  let numericValue = 0.3;
  let nonAlphanumericValue = 0.5;
  let totalNumeric = (password.match(/[0-9]/g) || []).length;
  let totalAlphaLower = (password.match(/[a-z]/g) || []).length;
  let totalAlphaUpper = (password.match(/[A-Z]/g) || []).length;
  let totalNonAlphanumeric = (password.match(/[^0-9a-zA-z]/g) || []).length;
  let repetitions = {};

  for (let i = 0; i < password.length; i++) {
    let penalty = 1;
    let character = password.charAt(i);
    repetitions[character] = repetitions[character] || 0;
    repetitions[character] = repetitions[character] + 1;

    if (repetitions[character] > 4) {
      penalty = 0.1;
    }

    if (/\d/.test(character)) {
      score += penalty * (numericValue * (password.length / totalNumeric));
    } else if (/[a-z]/.test(character)) {
      score +=
        penalty * (alphaLowerValue * (password.length / totalAlphaLower));
    } else if (/[A-Z]/.test(character)) {
      score +=
        penalty * (alphaUpperValue * (password.length / totalAlphaUpper));
    } else if (/\W/.test(character)) {
      score +=
        penalty *
        (nonAlphanumericValue * (password.length / totalNonAlphanumeric));
    }
  }

  if (options.checkForCommon) {
    for (let i = 0; i < commonPasswords.length; i++) {
      if (
        password.toLowerCase().indexOf(commonPasswords[i].toLowerCase()) !== -1
      ) {
        score *= 0.5;
        break;
      }
    }
  }

  return score;
}
