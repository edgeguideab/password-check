## Install via NPM

```
npm install @edgeguideab/password-check
```

## In the browser

You will need to require the module and then package your scripts using a bundler like webpack or browserify.

## Usage

### evaluate passwords

```javascript
const pwCheck = require('@edgeguideab/password-check');
let result = pwCheck.evaluate('mynicepassword123!');

console.log(result);
// { valid: true }
```

### add options to your evaluation

```javascript
const pwCheck = require('@edgeguideab/password-check');
let result = pwCheck.evaluate('mynicepassword123!', {
  length: 16,
  allowCommon: false,
  strict: true,
  numeric: 4,
  nonAlphaNumeric: 2,
  minScore: 12
});

console.log(result);
//{ valid: false, reason: 'CONTAINS_COMMON_PATTERNS' }
```

#### Options

| Name                | Type    | Description                                                                                 |
| ------------------- | ------- | ------------------------------------------------------------------------------------------- |
| length              | Number  | The minimum length a password has to be to be valid                                         |
| allowCommon         | Boolean | Invalidates all passwords matching one of the most common 1500 passwords                    |
| strict              | Boolean | Invalidates all passwords containing (as a substring) one of the most common 1500 passwords |
| upperCase / capital | Number  | The minimum number of capital characters needed for a password to be valid                  |
| lowerCase / small   | Number  | The minimum number of small characters needed for a password to be valid                    |
| numeric             | Number  | The minimum number of numeric characters for a password to be valid                         |
| nonAlphanumeric     | Number  | The minimum number of non-alphanumeric characters for a password to be valid                |
| minScore            | Number  | The minimum score the password needs to yield for it to be accepted                         |

#### Errors

These errors are reported back from the `evaluate` function. They are pretty self explanatory, but here goes.
| Name | Description |
|-----------------|-----------------------------------------------------------------|
| TOO_COMMON | The password was found among the 1500 most common passwords |
| TOO_FEW_CAPITAL_LETTERS | The password contained too few capital characters |
| TOO_FEW_SMALL_LETTERS | The password contained too few small characters |
| CONTAINS_COMMON_PATTERNS | A part of the password was found among the 1500 most common |
| TOO_FEW_NUMERIC_CHARACTERS | The password contained too few numeric characters (only with the numeric option) |
| TOO_FEW_NON_ALPHANUMERIC_CHARACTERS | The password contained too few alphanumeric characters (only with the alphanumeric option) |
| SCORE_TOO_LOW | The password scored too low |
| TOO_SHORT | The password was too short |

### score passwords

```javascript
const pwCheck = require('@edgeguideab/password-check');
let score = pwCheck.score('mynicepassword123!');
console.log(score);
// 16.2
```

### score with options

```javascript
const pwCheck = require('@edgeguideab/password-check');
let score = pwCheck.score('mynicepassword123!', {
  checkForCommon: true
});
console.log(score);
// 8.1
```

The scoring function will impose a penalty for passwords containing words from the 1500 most commonly used passwords if the checkForCommon flag is set to true. The default is false.

## Acknowledgements

All passwords in the top 1500 most common list were taken from the list provided by Koshevoy Dmitry at [passwordrandom.com](http://www.passwordrandom.com/)

## Author

[EdgeGuide AB](http://www.edgeguide.com)
